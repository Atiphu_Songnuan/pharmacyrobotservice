const express = require("express");
const app = express();
const hostname = "localhost";
const port = 3366;

app.get("/", (req, res) => {
    res.send("OK");
});

app.listen(port, hostname, () =>
  console.log(`listening at http://localhost:${port}`)
);
