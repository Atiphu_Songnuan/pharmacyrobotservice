const Fastify = require("fastify");
const jwt = require("jsonwebtoken");

var log4js = require("log4js");
log4js.configure({
  appenders: { service: { type: "file", filename: "service.log" } },
  categories: { default: { appenders: ["service"], level: "error" } },
});

const logger = log4js.getLogger("service");
// logger.trace("Entering service testing");
// logger.debug("Got service.");
// logger.info("service is Comté.");
// logger.warn("service is quite smelly.");
// logger.error("service is too ripe!");
// logger.fatal("Cheese was breeding ground for listeria.");

const hostname = "localhost";
const port = 5555;

const fastifyAPI = Fastify({
  logger: true,
});

fastifyAPI.post("/api/pharmacy/token", async (request, reply) => {
  let body = request.body;
  let user = body.username;
  let pass = body.password;
  let tokenresult;
  if (user === "admin" && pass === "*medpsu1951*") {
    let token = jwt.sign(
      {
        data: body.username,
      },
      "*robotpharmacy1951*",
      { expiresIn: "1m" }
    );
    tokenresult = {
      statusCode: 200,
      message: "success",
      type: "Bearer",
      token: token,
    };
    logger.info(tokenresult);
  } else {
    tokenresult = {
      statusCode: 400,
      message: "failed",
    };
    logger.error(tokenresult);
  }
  reply.send(tokenresult);
});

fastifyAPI.get("/api/pharmacy/order", async (request, reply) => {
  let orderResult;
  let bearerToken = request.headers.authorization;
  let token = bearerToken.split(" ")[1];

  jwt.verify(token, "*robotpharmacy1951*", (err, decoded) => {
    if (err) {
      orderResult = {
        status: false,
        message: "Token is expired!",
      };
      logger.error(orderResult);
    } else {
      orderResult = {
        status: true,
        data_drug_robot: [
          {
            DISP_DATE: "2004-02-19",
            DISP_TIME: "10:37:44",
            LANE: "11",
            DAILY_NO: "9",
            LIST_NUM: "2",
            LIST_TOTAL: "4",
            HN: "0447324",
            NAME: "นายศักรินทร์   บุญกาญจน์",
            LOCATION: "03Cc",
            DRUG_CODE: "851",
            DRUG_NAME_EN: "RanitiDINE 150 mg Tab",
            DRUG_NAME_TH: "รานิทิดีน 150  มก.",
            DRUG_DESCRIPTION: "แผงทึบเขียว เม็ดกลมขาว150",
            DRUG_AMT: "20",
            DRUG_UNIT: "TAB",
            DRUG_PERPACK: "1",
            DRUG_UNITPACK: "TAB",
            DRUG_USE_LINE1: "กิน ครั้งละ 1 เม็ด",
            DRUG_USE_LINE2: "วันละ 2 ครั้ง หลังอาหาร",
            DRUG_USE_LINE3: "เช้า เย็น",
            DRUG_USE_LINE4: "",
            DRUG_USE_LINE5: "",
            DRUG_USE_LINE6: "",
            DRUG_RDU_LINE1: "",
            DRUG_RDU_LINE2: "",
            DRUG_RDU_LINE3: "",
            DRUG_RDU_LINE4: "",
            DRUG_ID: "",
            LOT_NO: "",
            EXP_DATE: "0000-00-00",
            TMT_ID: "105868",
            GEN_LABEL:
              "pUvdY7SSGVvaNBv7ZKYEEA==|13779|200402191037|2/4|105868|20|1101:1",
            GEN_DOCNO: "20040219110009044732400851",
            GEN_DRUGID: "105868",
            GEN_OTHER: "",
          },
          {
            DISP_DATE: "2004-02-19",
            DISP_TIME: "10:37:44",
            LANE: "11",
            DAILY_NO: "9",
            LIST_NUM: "3",
            LIST_TOTAL: "4",
            HN: "0447324",
            NAME: "นายศักรินทร์   บุญกาญจน์",
            LOCATION: "03Ad",
            DRUG_CODE: "879",
            DRUG_NAME_EN: "Domperidone 10mg tab",
            DRUG_NAME_TH: "ดอมเพอริโดน",
            DRUG_DESCRIPTION: "แผงใสเงินเขียวเม็ดกลมขาวบาก",
            DRUG_AMT: "30",
            DRUG_UNIT: "TAB",
            DRUG_PERPACK: "1",
            DRUG_UNITPACK: "TAB",
            DRUG_USE_LINE1: "กิน ครั้งละ 1 เม็ด",
            DRUG_USE_LINE2: "วันละ 3 ครั้ง ก่อนอาหาร ครึ่งชม.",
            DRUG_USE_LINE3: "เช้า เที่ยง เย็น ",
            DRUG_USE_LINE4: "",
            DRUG_USE_LINE5: "",
            DRUG_USE_LINE6: "",
            DRUG_RDU_LINE1: "ช่วยลดอาการจุกเสียด คลื่นไส้ อาเจียน ท้องอืด",
            DRUG_RDU_LINE2: "ไม่ควรใช้เกินกว่าขนาดที่ระบุ",
            DRUG_RDU_LINE3: "",
            DRUG_RDU_LINE4: "",
            DRUG_ID: "",
            LOT_NO: "",
            EXP_DATE: "0000-00-00",
            TMT_ID: "715980",
            GEN_LABEL:
              "pUvdY7SSGVvaNBv7ZKYEEA==|13779|200402191037|3/4|715980|30|1101:1",
            GEN_DOCNO: "20040219110009044732400879",
            GEN_DRUGID: "715980",
            GEN_OTHER: "",
          },
          {
            DISP_DATE: "2004-02-19",
            DISP_TIME: "10:37:44",
            LANE: "11",
            DAILY_NO: "9",
            LIST_NUM: "1",
            LIST_TOTAL: "4",
            HN: "0447324",
            NAME: "นายศักรินทร์   บุญกาญจน์",
            LOCATION: "13Cb",
            DRUG_CODE: "1485",
            DRUG_NAME_EN: "Hyoscine(20mg/ml) Inj",
            DRUG_NAME_TH: "ไฮออสซีน 20มก./มล.",
            DRUG_DESCRIPTION: "\u003cHyoscine-N\u003e",
            DRUG_AMT: "1",
            DRUG_UNIT: "AMP",
            DRUG_PERPACK: "1",
            DRUG_UNITPACK: "AMP",
            DRUG_USE_LINE1: "ฉีด ครั้งละ 1 AmpIM",
            DRUG_USE_LINE2: "",
            DRUG_USE_LINE3: "",
            DRUG_USE_LINE4: "",
            DRUG_USE_LINE5: "",
            DRUG_USE_LINE6: "",
            DRUG_RDU_LINE1: "",
            DRUG_RDU_LINE2: "",
            DRUG_RDU_LINE3: "",
            DRUG_RDU_LINE4: "",
            DRUG_ID: "",
            LOT_NO: "",
            EXP_DATE: "0000-00-00",
            TMT_ID: "767686",
            GEN_LABEL:
              "pUvdY7SSGVvaNBv7ZKYEEA==|13779|200402191037|1/4|767686|11|101:1",
            GEN_DOCNO: "20040219110009044732401485",
            GEN_DRUGID: "767686",
            GEN_OTHER: "",
          },
          {
            DISP_DATE: "2004-02-19",
            DISP_TIME: "10:37:44",
            LANE: "11",
            DAILY_NO: "9",
            LIST_NUM: "4",
            LIST_TOTAL: "4",
            HN: "0447324",
            NAME: "นายศักรินทร์   บุญกาญจน์",
            LOCATION: "04Cd",
            DRUG_CODE: "1595",
            DRUG_NAME_EN: "Aluminium+Magnesium 240ml",
            DRUG_NAME_TH: "อะลูมิเนียม+แม็กนเซียม",
            DRUG_DESCRIPTION: "",
            DRUG_AMT: "1",
            DRUG_UNIT: "BOT",
            DRUG_PERPACK: "1",
            DRUG_UNITPACK: "BOT",
            DRUG_USE_LINE1: "กิน ครั้งละ 1 ช้อนโต๊ะ",
            DRUG_USE_LINE2: "วันละ 4 ครั้ง หลังอาหาร",
            DRUG_USE_LINE3: "เช้า เที่ยง เย็น ก่อนนอน",
            DRUG_USE_LINE4: "",
            DRUG_USE_LINE5: "",
            DRUG_USE_LINE6: "",
            DRUG_RDU_LINE1: "",
            DRUG_RDU_LINE2: "",
            DRUG_RDU_LINE3: "",
            DRUG_RDU_LINE4: "** เขย่าขวดก่อนรับประทาน **",
            DRUG_ID: "",
            LOT_NO: "",
            EXP_DATE: "0000-00-00",
            TMT_ID: "960272",
            GEN_LABEL:
              "pUvdY7SSGVvaNBv7ZKYEEA==|13779|200402191037|4/4|960272|1|1101:1",
            GEN_DOCNO: "20040219110009044732401595",
            GEN_DRUGID: "960272",
            GEN_OTHER: "",
          },
          {
            DISP_DATE: "2019-05-24",
            DISP_TIME: "10:37:26",
            LANE: "25",
            DAILY_NO: "1",
            LIST_NUM: "3",
            LIST_TOTAL: "3",
            HN: "0447324",
            NAME: "นายศักรินทร์   บุญกาญจน์",
            LOCATION: "99",
            DRUG_CODE: "1516",
            DRUG_NAME_EN: "Mabthera(500mg/50ml) Inj",
            DRUG_NAME_TH: "แมบทีรา 500มก./มล.",
            DRUG_DESCRIPTION: "",
            DRUG_AMT: "1",
            DRUG_UNIT: "VIAL",
            DRUG_PERPACK: "1",
            DRUG_UNITPACK: "VIAL",
            DRUG_USE_LINE1: "600 mg + NSS 250 ml IV Drip",
            DRUG_USE_LINE2: "ชั่วโมงที่ 1 Rate 20 ml/hr",
            DRUG_USE_LINE3: "ชั่วโมงที่ 2 Rate 40 ml/hr",
            DRUG_USE_LINE4: "ชั่วโมงถัดไป Rate 80 ml/hr",
            DRUG_USE_LINE5: "",
            DRUG_USE_LINE6: "",
            DRUG_RDU_LINE1: "",
            DRUG_RDU_LINE2: "",
            DRUG_RDU_LINE3: "",
            DRUG_RDU_LINE4: "** ห้าม drip ยาเร็ว และเก็บยาในตู้เย็น **",
            DRUG_ID: "",
            LOT_NO: "",
            EXP_DATE: "0000-00-00",
            TMT_ID: "653187",
            GEN_LABEL:
              "pUvdY7SSGVvaNBv7ZKYEEA==|13779|201905241037|3/3|653187|1|1101:1",
            GEN_DOCNO: "20190524250001044732401516",
            GEN_DRUGID: "653187",
            GEN_OTHER: "",
          },
          {
            DISP_DATE: "2019-05-24",
            DISP_TIME: "10:37:26",
            LANE: "25",
            DAILY_NO: "1",
            LIST_NUM: "1",
            LIST_TOTAL: "3",
            HN: "0447324",
            NAME: "นายศักรินทร์   บุญกาญจน์",
            LOCATION: "ค_11Af",
            DRUG_CODE: "2042",
            DRUG_NAME_EN: "5FU 1000 mg Inj",
            DRUG_NAME_TH: "5 เอฟยู 1000มก.",
            DRUG_DESCRIPTION: "",
            DRUG_AMT: "1",
            DRUG_UNIT: "VIAL",
            DRUG_PERPACK: "1",
            DRUG_UNITPACK: "VIAL",
            DRUG_USE_LINE1: "ฉีด ครั้งละ 2 mg ID",
            DRUG_USE_LINE2: "วันละ 3 ครั้ง ",
            DRUG_USE_LINE3: "เช้า เที่ยง เย็น ",
            DRUG_USE_LINE4: "",
            DRUG_USE_LINE5: "",
            DRUG_USE_LINE6: "",
            DRUG_RDU_LINE1: "",
            DRUG_RDU_LINE2: "",
            DRUG_RDU_LINE3: "",
            DRUG_RDU_LINE4: "",
            DRUG_ID: "",
            LOT_NO: "",
            EXP_DATE: "0000-00-00",
            TMT_ID: "667410",
            GEN_LABEL:
              "pUvdY7SSGVvaNBv7ZKYEEA==|13779|201905241037|1/3|667410|1|1101:1",
            GEN_DOCNO: "20190524250001044732402042",
            GEN_DRUGID: "667410",
            GEN_OTHER: "",
          },
        ],
      };
      logger.info(orderResult);
      // reply.header('Content-Type', 'application/json; charset=utf-8').send(result);
    }
    reply.send(orderResult);
  });
});

fastifyAPI.post(
  "/api/pharmacy/pharmacy/updatestatus",
  async (request, reply) => {
    console.log(request.body);
    let result = {
      statusCode: 200,
      message: "Update Success",
    };
    reply.send(result);
  }
);

fastifyAPI.listen(port, hostname, () => {
  console.log(`server is running on ${port}`);
});
